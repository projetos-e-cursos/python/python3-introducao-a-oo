# Python3: introducao a OO

## Curso de python3, introdução a Orientação a Objetos.

### Conceitos abordados:

- Paradigma Orientação a Objetos;
- Diferenças entre programação estrutural e orientada a objetos;
- Classes;
- Atributos e Métodos;
- Encapsulamento;
- Atributos e Métodos privados;
- Função construtora;
- Objetos em Python (em Python tudo é __OBJETO__);
- Endereço e referência de objetos;
- Coesão do código;
- Métodos de acesso a atributos da classe (getters e setters);
- Simplificando métodos de acesso usando @property e @nome.setter;
- Métodos da classe (@staticmethod).

![programe-orientado-a-objetos](/uploads/9e74c60b815f9c33d177c095d2e572da/programe-orientado-a-objetos.png)