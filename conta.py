

class Conta:

    def __init__(self, numero, titular, saldo, limite):
        self.__numero = numero
        self.__titular = titular
        self.__saldo = saldo
        self.__limite = limite

    def __repr__(self):
        return f'Titular:{self.__titular} | Saldo: {self.__saldo}'

    def depositar(self, valor):
        self.__saldo += valor

    def __validar_saque(self, valor_a_sacar):
        valor_disponivel = self.__saldo + self.__limite
        return valor_a_sacar <= valor_disponivel

    def sacar(self, valor):
        if self.__validar_saque(valor):
            self.__saldo -= valor
        else:
            print(f'O valor {valor} passou o limite!')

    def imprimir_extrato(self):
        print(f'Saldo: {self.__saldo}, Titular: {self.__titular}')

    def transferir(self, valor, conta):
        self.sacar(valor)
        conta.depositar(valor)

    @property
    def saldo(self):
        return self.__saldo

    @property
    def titular(self):
        return str(self.__titular).title()

    @property
    def numero(self):
        return self.__numero

    @property
    def limite(self):
        return self.__limite

    @limite.setter
    def limite(self, limite):
        self.__limite = limite

    @staticmethod
    def codigo_banco():
        return '001'

    @staticmethod
    def codigos_bancos():
        return {'BB': '001', 'Caixa': '104', 'Bradesco': '237'}
